// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(1, 2)), make_tuple(1, 2, 2));
}

TEST(CollatzFixture, eval5) {
    ASSERT_EQ(collatz_eval(make_pair(1, 999999)), make_tuple(1, 999999, 525));
}

TEST(CollatzFixture, eval6) {
    ASSERT_EQ(collatz_eval(make_pair(990000, 999999)), make_tuple(990000, 999999, 440));
}

//unit tests for find_cycles

TEST(CollatzFixture, unit1) {
    ASSERT_EQ(find_cycles(1), 1);
}

TEST(CollatzFixture, unit2) {
    ASSERT_EQ(find_cycles(100), 26);
}

TEST(CollatzFixture, unit3) {
    ASSERT_EQ(find_cycles(1000), 112);
}

TEST(CollatzFixture, unit4) {
    ASSERT_EQ(find_cycles(43), 30);
}

TEST(CollatzFixture, unit5) {
    ASSERT_EQ(find_cycles(9000), 48);
}

TEST(CollatzFixture, unit6) {
    ASSERT_EQ(find_cycles(837799), 525);
}

TEST(CollatzFixture, unit7) {
    ASSERT_EQ(find_cycles(2), 2);
}


//find_mcl unit tests

TEST(CollatzFixture, unit9) {
    ASSERT_EQ(find_mcl(978600, 373542), 525);
}

TEST(CollatzFixture, unit10) {
    ASSERT_EQ(find_mcl(8023, 6429), 257);
}

TEST(CollatzFixture, unit11) {
    ASSERT_EQ(find_mcl(784, 695), 171);
}

// find_mcl_cache unit tests

TEST(CollatzFixture, unit13) {
    ASSERT_EQ(find_mcl_cache(978600, 373542), 525);
}

TEST(CollatzFixture, unit14) {
    ASSERT_EQ(find_mcl_cache(8023, 6429), 257);
}

TEST(CollatzFixture, unit15) {
    ASSERT_EQ(find_mcl_cache(784, 695), 171);
}

TEST(CollatzFixture, unit16) {
    ASSERT_EQ(find_mcl_cache(1, 3000), 217);
}

TEST(CollatzFixture, unit17) {
    ASSERT_EQ(find_mcl_cache(1000, 3000), 217);
}

TEST(CollatzFixture, unit18) {
    ASSERT_EQ(find_mcl_cache(1000, 2999), 217);
}

TEST(CollatzFixture, unit19) {
    ASSERT_EQ(find_mcl_cache(1, 10), 20);
}

TEST(CollatzFixture, unit20) {
    ASSERT_EQ(find_mcl_cache(2, 10), 20);
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}
