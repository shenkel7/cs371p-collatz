// ---------
// Collatz.h
// ---------

#ifndef Collatz_h
#define Collatz_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <tuple>    // tuple
#include <utility>  // pair

using namespace std;

// ------------
// collatz_read
// ------------

/**
 * read two ints
 * @param a string
 * @return a pair of ints
 */
pair<int, int> collatz_read (const string&);

// ------------
// collatz_eval
// ------------

/**
 * @param a pair of ints
 * @return a tuple of three ints
 */
tuple<int, int, int> collatz_eval (const pair<int, int>&);

// -------------
// collatz_print
// -------------

/**
 * print three ints
 * @param an ostream
 * @param a tuple of three ints
 */
void collatz_print (ostream&, const tuple<int, int, int>&);

// -------------
// collatz_solve
// -------------

/**
 * @param an istream
 * @param an ostream
 */
void collatz_solve (istream&, ostream&);

// ------------
// find_cycles
//computes number of collatz conjecture cycles for a given number
// ------------

/**
 * @param number
 * @return cycle length of number
 */
int find_cycles(long);


// ------------
// find_mcl
//computes max cycle length given a range
// ------------

/**
 * @param a pair of ints i, j marking range
 * @return max cycle length in range
 */

int find_mcl(int, int);


// ------------
// find_mcl_cache
// returns mcl in range [i, j) with cache lookup
// ------------

/**
 * @param two numbers i, j marking range 
 * @return max cycle length of a number in range
 */

int find_mcl_cache(int, int);

#endif // Collatz_h
